<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <?php print $styles ?>
    <script type="text/javascript" src="/<?php print drupal_get_path('module', 'node_relations') . '/js/node_relations.js' ?>"></script>
    <?php print $scripts ?>
    
    <title><?php print $head_title ?></title>
  </head>
  <body <?php print drupal_attributes($attr) ?>>

  <div id='page'>
    <?php if ($tabs2): ?><div class='secondary-tabs clear-block'><?php print $tabs2 ?></div><?php endif; ?>
    <?php if ($help) print $help ?>
    <div class='page-content clear-block'>
      <?php if ($show_messages && $messages): ?>
        <div id='console' class='clear-block'><?php print $messages; ?></div>
      <?php endif; ?>

      <div id='content'>
        <?php if (!empty($content)): ?>
          <div class='content-wrapper clear-block'><?php print $content ?></div>
        <?php endif; ?>
      </div>
    </div>
  </div>

  <?php if ($footer_message): ?><div class='footer-message'><?php print $footer_message ?></div><?php endif; ?>

  <?php print $closure ?>

  </body>
</html>

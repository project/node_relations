node_relations_transfer = function (response, status) {

  var new_content = $('<div></div>', window.parent.document).html(response.data);
  parent.Drupal.freezeHeight();
  var wrapper = $('#node-relations-wrapper', window.parent.document);
  wrapper.empty().append(new_content);
  new_content.show();

  // Attach all javascript behaviors to the new content, if it was successfully
  // added to the page, this if statement allows #ahah[wrapper] to be optional.
  if (new_content.parents('html').length > 0) {
    parent.Drupal.attachBehaviors(new_content);
  }

  parent.Drupal.unfreezeHeight();
  
};
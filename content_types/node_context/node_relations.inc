<?php

$plugin = array(
  'single' => TRUE,
  'title' => t('Node relations'),
  'icon' => 'icon_node.png',
  'description' => t('Node relations of the referenced node.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'category' => t('Node'),
  'defaults' => array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'identifier' => '',
  ),
);

/**
 * Output function for the 'node' content type. Outputs a node
 * based on the module and delta supplied in the configuration.
 */
function node_relations_node_relations_content_type_render($subtype, $conf, $panel_args, $context) {
  if (!empty($context) && empty($context->data)) {
    return;
  }

  $node = isset($context->data) ? drupal_clone($context->data) : NULL;

  $block = new stdClass();
  $block->module = 'node_relations';
  $block->delta  = $node->nid;

  if (!empty($node->node_relations)) {
    if (!empty($conf['identifier'])) {
      $node->panel_identifier = $conf['identifier'];
    }

    $block->subject = '';
    $block->content = theme('node_relations_all', $node->node_relations);
  }

  return $block;
}

/**
 * Returns an edit form for the custom type.
 */
function node_relations_node_relations_content_type_edit_form(&$form, &$form_state) {
  $conf = $form_state['conf'];

  $form['identifier'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['identifier'],
    '#title' => t('Identifier'),
    '#description' => t('Whatever is placed here will appear in $node->panel_identifier to help theme node links displayed on the panel'),
  );

  return $form;
}

function node_relations_node_relations_content_type_edit_form_submit(&$form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

function node_relations_node_relations_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" relations', array('@s' => $context->identifier));
}

